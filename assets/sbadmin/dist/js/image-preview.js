
/* 
 * Image Preview 1 
 * */

$(function() {
    $('#img-preview').click(function() {
      $('#img-input').trigger('click');
    });

    $("#img-input").change(function() {  
      readImage(this);
    });

    var readImage = function(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          var html = '<img src="' + e.target.result + '" class="image-preview" id="image-preview">';
          $('#img-preview').html(html);
          var pic_bar ='<span id="delete_picture" class="delete_picture"></span>';
          $('#picture_bar').html(pic_bar);
        };

        reader.readAsDataURL(input.files[0]);
      };
    };    
});

/* 
 * Image Preview 2 
 * */

$(function() {
    $('#img-preview2').click(function() {
      $('#img-input2').trigger('click');
    });

    $("#img-input2").change(function() {  
      readImage(this);
    });

    var readImage = function(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          var html = '<img src="' + e.target.result + '" class="image-preview" id="image-preview2">';
          $('#img-preview2').html(html);
          var pic_bar ='<span id="delete_picture2" class="delete_picture2"></span>';
          $('#picture_bar2').html(pic_bar);
        };

        reader.readAsDataURL(input.files[0]);
      }
    };      
});

/* 
 * Image Preview 3
 * */

$(function() {
    $('#img-preview3').click(function() {
      $('#img-input3').trigger('click');
    });

    $("#img-input3").change(function() {  
      readImage(this);
    });

    var readImage = function(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          var html = '<img src="' + e.target.result + '" class="image-preview" id="image-preview3">';
          $('#img-preview3').html(html);
          var pic_bar ='<span id="delete_picture3" class="delete_picture3"></span>';
          $('#picture_bar3').html(pic_bar);
        };

        reader.readAsDataURL(input.files[0]);
      }
    };      
});

/* 
 * Image Preview 4
 * */

$(function() {
    $('#img-preview4').click(function() {
      $('#img-input4').trigger('click');
    });

    $("#img-input4").change(function() {  
      readImage(this);
    });

    var readImage = function(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          var html = '<img src="' + e.target.result + '" class="image-preview" id="image-preview4">';
          $('#img-preview4').html(html);
          var pic_bar ='<span id="delete_picture4" class="delete_picture4"></span>';
          $('#picture_bar4').html(pic_bar);
        };

        reader.readAsDataURL(input.files[0]);
      }
    };      
});

/* 
 * Image Preview 5
 * */

$(function() {
    $('#img-preview5').click(function() {
      $('#img-input5').trigger('click');
    });

    $("#img-input5").change(function() {  
      readImage(this);
    });

    var readImage = function(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          var html = '<img src="' + e.target.result + '" class="image-preview" id="image-preview5">';
          $('#img-preview5').html(html);
          var pic_bar ='<span id="delete_picture5" class="delete_picture5"></span>';
          $('#picture_bar5').html(pic_bar);
        };

        reader.readAsDataURL(input.files[0]);
      }
    };      
});

/* 
 * Image Preview 6
 * */

$(function() {
    $('#img-preview6').click(function() {
      $('#img-input6').trigger('click');
    });

    $("#img-input6").change(function() {  
      readImage(this);
    });

    var readImage = function(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          var html = '<img src="' + e.target.result + '" class="image-preview" id="image-preview6">';
          $('#img-preview6').html(html);
          var pic_bar ='<span id="delete_picture6" class="delete_picture6"></span>';
          $('#picture_bar6').html(pic_bar);
        };

        reader.readAsDataURL(input.files[0]);
      }
    };      
});

$(document).on('click','.delete_picture',function(){
     $('#image-preview').remove();
     $('#delete_picture').remove();
});

$(document).on('click','.delete_picture2',function(){
     $('#image-preview2').remove();
     $('#delete_picture2').remove();
});

$(document).on('click','.delete_picture3',function(){
     $('#image-preview3').remove();
     $('#delete_picture3').remove();
});

$(document).on('click','.delete_picture4',function(){
     $('#image-preview4').remove();
     $('#delete_picture4').remove();
});

$(document).on('click','.delete_picture5',function(){
     $('#image-preview5').remove();
     $('#delete_picture5').remove();
});

$(document).on('click','.delete_picture6',function(){
     $('#image-preview6').remove();
     $('#delete_picture6').remove();
});