<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller{
	
	public function __construct(){
		
		parent::__construct();
		//if(!$this->session->userdata('is_login'))redirect('login');
		$this->session->set_userdata('menu','');	
	}
	
	public function index(){
	    
	    $data = array('title'=>'Aplikasi Penjualan dan Stok | candrasa.com');
	    
	    $this->load->view('header',$data);
	    $this->load->view('sidemenu');
	    $this->load->view('dashboard');
	    $this->load->view('footer');
		
	}


}
