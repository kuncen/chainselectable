<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends MX_Controller{
    
    public $d;
    public function __construct(){
        
        parent::__construct();
        $this->load->model('product_model');
        $this->load->library('MY_Form_validation');
        //$this->output->enable_profiler(TRUE);
        $this->_mod_url = base_url().'product/product/';
    }   
    
    private function _render($view,$data){
        
//        $data['parent_category']    = $this->category_model->get_parentCategory();
//        $category                   = $this->category_model->get_productCategory(0);
//        $tree                       = $this->prepareList($category);
        $data['parent_category']     = $this->product_model->get_Category(0);
        $data['sub_category']        = $this->product_model->get_Category(1);
        $data['prod_unit']           = $this->product_model->get_prodUnit();
        $data['product_table']       = $this->get_productTable();
        
        $this->load->view('header');
        $this->load->view('sidemenu');
        $this->load->view($view,$data);      
        $this->load->view('footer');
        
    }
    
    public function index(){
          
           $this->_render('product', $this->d);
        
    }
    
    public function add_product(){
        $this->form_validation("add");
        $this->_render('add_product', $this->d);
        
    }
    
    function get_productTable(){

        $product = $this->product_model->get_productList();
        $output="";
        foreach ($product as $item) {
            $output .= '<tr class="gradeA">';
            $output .= '<td> <a href="'.$this->_mod_url.'index/edit-product/'.$item['product_id'].'">'.$item['product_id'].'</a> </td>';
            $output .= '<td> '.$item["product_name"].'</td>';
            $output .= '<td> '.$item["product_desc_short"].'</td>';
            $output .= '<td> '.$item["category_name"].'</td>';
            $output .= '<td> '.$item["product_weight"].'</td>';
            $output .= '<td> <a title="Delete" class="text-danger" data-toggle="modal" data-target="#myModal" onclick="confirm_modal(\''.$this->_mod_url.'do-delete/'.$item["product_id"].'\',\''.$item['product_name'].' category\')" href="#">';
            $output .= '<i class="fa fa-times"></i> Delete </a> </td>';            
            $output .= '</tr>';
        }          
               
        return $output;
    }
    public function do_save() {
        
        $this->form_validation();
        
        //if($this->input->post('select_sub') == ""){
        //    $category_id = $this->input->post('select_category');
        //}
        //else{
        //   $category_id = $this->input->post('select_sub');
        //}
        
        if ($this->form_validation->run() == FALSE){
            $this->d['val'] = 'FAILED';
            $this->d['form'] ='add-product';
            $this->_render('add_product',$this->d);
        }
        else{
            $data = array (
                'product_name'              => $this->input->post('product_name'),
                'product_weight'            => $this->input->post('product_weight'),
                'product_weight_packaging'  => $this->input->post('product_weight_packaging'),
                'product_order_code'        => $this->input->post('product_order_code'),
                'unit_id'                   => $this->input->post('select_unit'),
                'category_id'               => $this->input->post('select_category'),
                'product_desc_short'        => $this->input->post('product_desc_short'),
                'product_desc_long'         => $this->input->post('product_desc_long')                         
                );     
            $result = $this->product_model->add_data($data);
            if($result){
                redirect($this->_mod_url.'index/a-s');
            }
        }      
    }
    public function do_update() {
        
        $this->form_validation();

        if ($this->form_validation->run() == FALSE){
            $this->d['val'] = 'FAILED';
            $this->d['form'] ='edit-category';
            $this->_render('category',$this->d);
        }
        else{
            $id = $this->input->post('category_id');            
            $data = array (
                'category_name' => $this->input->post('category_name'),
                'description'   => $this->input->post('category_description'),
                'parent_id'     => $this->input->post('select_parent')                
                );     
            $result = $this->category_model->update_data($id,$data);
            if($result){
                redirect($this->_mod_url.'index/edit-category/'.$this->input->post('category_id').'/u-s');
            }
        }      
    }
    
    public function do_delete(){
        
        $data = array ( 'category_id' => $this->uri->segment(4));
                        
       if ($this->category_model->delete_data($data) == true){       
            redirect($this->_mod_url.'index/d-s');       
       }
       else{
            redirect($this->_mod_url.'index/d-f');
       }
    }
    private function form_validation(){
//       $c = $this->_set_fields();
//        if($this->input->post('category_name') != $c['category_name']) {
//            $is_unique =  '|is_unique[category.category_name]';        
//         } else {
//            $is_unique =  '';
//         }
        $is_unique =  '';
        $this->form_validation->set_error_delimiters('<div><small>', '</small></div>');
        $this->form_validation->set_rules('product_name','Product Name','required|alpha_extra');     
        $this->form_validation->set_rules('product_weight','Product Weight','required|numeric');        
        $this->form_validation->set_rules('product_weight_packaging','Product Weight and Packaging','required|numeric');       
        $this->form_validation->set_rules('product_order_code','Order Code','alpha_extra');
        $this->form_validation->set_rules('product_desc_short','Short Product Description','alpha_extra');
        $this->form_validation->set_rules('product_desc_long','Product Description','');
        $this->form_validation->set_rules('category_id','','');  
        
        // Return the result validation to form
        $this->d['product_name']              = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('product_name'):"";
        $this->d['product_weight']            = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('product_weight'):"";        
        $this->d['product_weight_packaging']  = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('producta_weight_packaging'):"";
        $this->d['product_order_code']        = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('product_order_code'):"";
        $this->d['product_desc_short']        = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('product_desc_short'):"";
        $this->d['product_order_code']        = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('product_order_code'):"";
        $this->d['product_desc_long']         = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('product_desc_long'):"";

    }
    
    private function _set_fields($id){
        
        $product = $this->category_model->get_detailCategory($id);
        $this->d['product_name']                = $product->product_name;
        $this->d['product_weight']              = $product->product_weight;
        $this->d['product_weight_packaging']    = $product->producta_weight_packaging;
        $this->d['product_order_code']          = $product->product_order_code;
        $this->d['product_desc_short']          = $product->product_desc_short;
        $this->d['product_desc_long']           = $product->product_desc_long;
        
        return $this->d;
    }

}