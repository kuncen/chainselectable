<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MX_Controller{
    
    public $d;
    public function __construct(){
        
        parent::__construct();
        $this->load->model('category_model');
        $this->load->library('MY_Form_validation');
        //$this->output->enable_profiler(TRUE);
        $this->_mod_url = base_url().'product/category/';
    }   
    
    private function _render($view,$data){
        
        $data['parent_category']    = $this->category_model->get_category(0);
        $category                   = $this->category_model->get_category(0);
        $data['table_category']     = $this->set_categoryTable($category['rows'],'',0);
        $data['parent_select']      = $this->set_parentSelect($category['rows'],'',0);
        
        $this->load->view('header');
        $this->load->view('sidemenu');
        $this->load->view($view,$data);      
        $this->load->view('footer');
        
    }
    
    public function index(){
        
        $this->form_validation("add");
        $this->d['val'] = "";
        $this->d['form'] ="add-category";
        if($this->uri->segment(4,1)=="edit-category"){           
           $this->_set_fields($this->uri->segment(5,1));
           $this->d['form'] ="edit-category";
           $this->_render('category', $this->d);
        }
        else{
           $this->_render('category', $this->d);
        }
        
    }
    function prepareList($items, $pid = 0) {
        
        foreach ($items as $item) {
            if ($item->parent_id == $pid) {
                return true;              
            }
        }
        return false;
    }
    
    function set_parentSelect($category_items,$ch,$pid){
        
        $output = "";
        $optgrp = "";
        if (count($category_items)>0) {
            
            $pid===0 ? $ch .="" : $ch .="- ";

            foreach ($category_items as $item) {
                                   
                if($this->uri->segment(5,0) == false){
                   $disable='';
                   $select='';
                }
                else{
                    $category = $this->category_model->get_detailCategory($this->uri->segment(5,1));
                    $item->category_id == $category->parent_id ? $select = "selected" : $select="";     
                    if($item->category_id == $category->category_id){
                        $output .= " <optgroup disabled>";
                        $optgrp = "</optgroup>";
                    }
                    
                }
                $output .= "<option value='{$item->category_id}' {$select}>{$ch}{$item->category_name}</option>";
                
                //check if there are any children
                if ($this->prepareList($category_items,$item->parent_id)){                   
                    $items   = $this->category_model->get_category($item->category_id);
                    $output .= $this->set_parentSelect($items['rows'],$ch,$item->parent_id);
                }
                $output .= $optgrp;
            }          
        }       
        return $output;  
    }
    
    function set_categoryTable($category_items,$ch,$parent){
        
        $output = "";
        if (count($category_items)>0) {
            if($parent===0){
                $ch .="";
            }
            else{
                $ch .="- ";
            }
            foreach ($category_items as $item) {
                $output .= "<tr class='gradeA'>";
                $output .= "<td><a href='{$this->_mod_url}index/edit-category/{$item->category_id}'>{$ch}{$item->category_name}</a> </td>";
                $output .= "<td>{$item->description}</td>";
                $output .= "<td>{$this->category_model->count_productByCategory($item->category_id)}</td>";
                $output .= "<td> <a title='Delete' class='text-danger' data-toggle='modal' data-target='#myModal' onclick='confirm_modal(\"{$this->_mod_url}do-delete/{$item->category_id}\",\"{$item->category_name} category \")' href='#'>";
                $output .= "<i class='fa fa-times'></i> Delete </a> </td>";
                //check if there are any children
                if ($this->prepareList($category_items,$item->parent_id)){
                    
                    $items   = $this->category_model->get_category($item->category_id);
                    $output .= $this->set_categoryTable($items['rows'],$ch,$item->parent_id);
                }
                $output .= "</tr>";
            }          
        }       
        return $output;
    }
    
    
    public function do_save() {
        
        $this->form_validation("save");

        if ($this->form_validation->run() == FALSE){
            $this->d['val'] = "FAILED";
            $this->d['form'] ="add-category";
            $this->_render('category',$this->d);
        }
        else{
            $data = array (
                'category_name' => $this->input->post('category_name'),
                'description'   => $this->input->post('category_description'),
                'parent_id'     => $this->input->post('select_parent')                
                );     
            $result = $this->category_model->save_data($data);
            if($result){
                redirect($this->_mod_url.'index/a-s');
            }
        }      
    }
    public function do_update() {
        
        $cat_id = $this->input->post('category_id');
        $this->form_validation("update");

        if ($this->form_validation->run() == FALSE){
            $this->d['val'] = 'FAILED';
            $this->d['form'] ='edit-category';
            $this->_render('category',$this->d);
        }
        else{                       
            $data = array (
                'category_name' => $this->input->post('category_name'),
                'description'   => $this->input->post('category_description'),
                'parent_id'     => $this->input->post('select_parent')                
                );     
            $result = $this->category_model->update_data($cat_id,$data);
            if($result){
                redirect($this->_mod_url.'index/edit-category/'.$this->input->post('category_id').'/u-s');
            }
        }      
    }
    
    public function do_delete(){
        
        $data = array ( 'category_id' => $this->uri->segment(4));
                        
       if ($this->category_model->delete_data($data) == true){       
            redirect($this->_mod_url.'index/d-s');       
       }
       else{
            redirect($this->_mod_url.'index/d-f');
       }
    }
    private function form_validation($process){
        
        if ($process == "update"){
            
            $this->d['category_id'] = $this->form_validation->set_value('category_id');
            
            $c = $this->_set_fields($this->input->post('category_id'));
            
            if($this->input->post('category_name') != $c['category_name']) {
                $is_unique =  '|is_unique[category.category_name]';        
             } else {
                $is_unique =  '';
             }
             
        }
        else{            
            $is_unique =  '|is_unique[category.category_name]';
        }
        
        $this->form_validation->set_error_delimiters('<div><small>', '</small></div>');
        $this->form_validation->set_rules('category_name','Category Name','required|alpha_extra'.$is_unique);     
        $this->form_validation->set_rules('category_description','Category Description','alpha_extra');        
        $this->form_validation->set_rules('select_parent','Parent Category','required');       
                
        // Return the result validation to form
        $this->d['category_name']             = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('category_name'):"";
        $this->d['category_description']      = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('category_description'):"";        
        $this->d['parent_id']                 = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('select_parent'):"";
        
    }
    
    private function _set_fields($id){
        
        $category = $this->category_model->get_detailCategory($id);
        $this->d['category_name']           = $category->category_name;
        $this->d['category_description']    = $category->description;
        $this->d['parent_id']               = $category->parent_id;
        $this->d['category_id']             = $category->category_id;
        
        return $this->d;
    }

}