<!-- Product Category -->

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-10">
                <h1 class="page-header">Product Category</h1>
            </div>
             <div class="col-lg-2">            
                <h1 class="page-header" align="right">
                    <button type="button" class="btn btn-success" onclick="parent.location='<?php echo base_url();?>product/category'">+ Add New</button>
                </h1>                  
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5">
                <!-- cat -->
            <?php
                
                if($this->uri->segment(4,1) == 'edit-category' || $form == 'edit-category' ){
                   echo  $this->load->view('edit_category');
                }
                elseif($form == 'add-category'){
                   echo  $this->load->view('add_category');
                }
            ?>
            </div>
            <!-- /.col-lg-5 -->
            <div class="col-lg-7">              
                <!-- category-->
                <?php if($this->uri->segment(4,1)=='d-s'){ ?>
                <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                The product category has been successfully deleted
                </div>
                <?php } elseif($this->uri->segment(4,1)=='d-f'){ ?>
                <div class="alert alert-danger min-form">
                The product categories can't be deleted because it has a dependency to the sub category or product.
                </div>                              
                <?php  }?>
                
                <table class="table table-striped table-bordered table-hover" id="category">
                    <thead>
                        <tr>
                            <th class="no-sort">Category Name</th>
                            <th class="no-sort">Description</th>
                            <th class="no-sort">Product</th>
                            <th class="no-sort">Action</th>                                        
                        </tr>
                    </thead>
                    <tbody>                               
                        <?=$table_category;?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->                                                         
            </div>
        <!-- /.category --> 
        <!-- (Normal Modal)-->
        <div class="modal fade" id="modal_delete_m_n"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content" style="margin-top:100px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" style="text-align:center;">Are you sure to Delete this <span class="grt"></span> ?</h4>
                    </div>
                    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                        <span id="preloader-delete"></span>
                        </br>
                        <a class="btn btn-danger" id="delete_link_m_n" href="">Delete</a>
                        <button type="button" class="btn btn-info" data-dismiss="modal" id="delete_cancel_link">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /modal -->           
    </div>
</div>
    	
<!-- /# dashboard -->

<!-- jQuery -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/sbadmin/dist/js/sb-admin-2.js"></script>
    <script src="<?php echo base_url(); ?>assets/sbadmin/js/jquery.chained.min.js" type="text/javascript"></script>
    <!-- DataTables JavaScript -->

    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- validator JavaScript -->
    <script src="<?php echo base_url();?>assets/dist/validator.min.js"></script>
    <script src="<?php echo base_url();?>assets/dist/validator.js"></script>    
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/sbadmin/dist/js/sb-admin-2.js"></script>
    <!-- bootstrap-select -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        $(document).ready(function() {
           $('#category').dataTable( {
               "order": [],
               "columnDefs": [ {
                 "targets"  : 'no-sort',
                 "orderable": false
               }]
           });
       });

    	function confirm_modal(delete_url,title)
    	{
    		jQuery('#modal_delete_m_n').modal('show', {backdrop: 'static',keyboard :false});
    		jQuery("#modal_delete_m_n .grt").text(title);
    		document.getElementById('delete_link_m_n').setAttribute("href" , delete_url );
    		document.getElementById('delete_link_m_n').focus();
    	}
    	</script>
</body>
</html>