<?php if($this->uri->segment(6,1)=='u-s'){ ?>
    <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    Updating the categories have been successfully !!
</div>
<?php } elseif($this->uri->segment(6,1)=='u-f' || $val=='FAILED'){?>
<div class="alert alert-danger min-form">
    <h6><?=$this->lang->line('err_title');?></h6>
    <?php echo validation_errors(); ?>
</div>
<?php } ?>

<div class="panel panel-default">
    <div class="panel-heading">Edit Product Category</div>
    <div class="panel-body">
<!-- category-->               
        <form role="form" method="post" action="<?=base_url();?>product/category/do-update" data-toggle="validator">
            <input type="hidden" name="category_id" value="<?=$category_id;?>">
            <div class="form-group <?=(!empty(form_error('select_parent')))?'has-error':'';?>">
                <label for="select_parent" class="control-label">* Parent Category</label>
                <select class="form-control selectpicker" data-live-search="true" tabindex="7" name="select_parent" id="select_parent" required>
                    <option value="0"> as Root Category</option>            
                    <?=$parent_select;?>
                </select>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group <?=(!empty(form_error('category_name')))?'has-error':'';?>">
                <label for="CategoryName" class="control-label">* Category Name</label>
                <input type="text" class="form-control" tabindex="1" name="category_name" id="CategoryName" value="<?=$category_name;?>"  pattern="<?=MY_Form_validation::$alpha_extra_pattern;?>" data-error="<?=$this->lang->line('err_category_name');?>" required>
                <div class="help-block with-errors"></div>
            </div>
            
            <div class="form-group <?=(!empty(form_error('category_description')))?'has-error':'';?>">
                <label for="CategoryDescription" class="control-label">Description</label>
                <textarea class="form-control" tabindex="3" rows="4" name="category_description" id="CategoryDescription" data-error="<?=$this->lang->line('err_category_description');?>"><?=$category_description;?></textarea>
                <div class="help-block with-errors"></div>
            </div>                                
            <button tabindex="16" type="submit" class="btn btn-default">Update</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </form>
    </div>
<!-- /.category -->
</div>