 <!-- store form setting  -->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <?php if(isset($val) == 'FAILED'){?>
                <div class="alert alert-danger min-form">
                    <h6><?=$this->lang->line('err_title');?></h6>
                    <?php echo validation_errors(); ?>
                </div>
                <?php } elseif($this->uri->segment(4,1)=='s'){ ?>
                    <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    Store Profile has been updated successfully !!
                </div>
                <?php } ?>
                
                <form role="form" method="post" action="<?=base_url();?>product/product/do-save" data-toggle="validator">
                    <input type="hidden" name="update" value="update">
                    <div class="panel panel-default">                   
                        <div class="panel-heading"> Add New Product</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">                                    
                                    <div class="form-group <?=(!empty(form_error('product_name')))?'has-error':'';?>">
                                        <label for="ProductName" class="control-label">* Product Name</label>
                                        <input class="form-control" tabindex="2" name="product_name" id="ProductName" value="<?=$product_name;?>" pattern="<?=MY_Form_validation::$alpha_extra_pattern;?>" data-error="<?=$this->lang->line('err_product_name');?>" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group <?=(!empty(form_error('product_weight')))?'has-error':'';?>">
                                        <label for="ProductWeight" class="control-label">* Weight</label>
                                        <div class="input-group">
                                            <input aria-describedby="ProductWeight" class="form-control" tabindex="3" name="product_weight" id="ProductWeight" value="<?=$product_weight;?>" data-error="<?=$this->lang->line('err_product_weight');?>" maxlength="5" required>
                                            <span class="input-group-addon" id="Weight">gram</span>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group <?=(!empty(form_error('product_weight_packaging')))?'has-error':'';?>">
                                        <label for="ProductWeightPackaging" class="control-label">* Weight + Packaging</label>
                                        <div class="input-group">
                                            <input aria-describedby="ProductWeightPackaging" class="form-control" tabindex="4" name="product_weight_packaging" id="ProductWeightPackaging" value="<?=$product_weight_packaging;?>" data-error="<?=$this->lang->line('err_product_weight_packaging');?>" maxlength="5" required>
                                            <span class="input-group-addon" id="WeightPackaging">gram</span>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group <?=(!empty(form_error('producta_order_code')))?'has-error':'';?>">
                                        <label for="ProductOrderCode" class="control-label">Order Code</label>
                                        <input aria-describedby="ProductOrderCode" class="form-control" tabindex="5" name="product_order_code" id="ProductOrderCode" value="<?=$product_order_code;?>" data-error="<?=$this->lang->line('err_product_order_code');?>">
                                        <div class="help-block with-errors"></div>
                                    </div>  
                                    <div class="form-group <?=(!empty(form_error('select_unit')))?'has-error':'';?>">
                                        <label for="select_unit" class="control-label">* Unit </label>
                                        <select class="form-control selectpicker" name="select_unit" tabindex="6" data-live-search="true" required>
                                            <option value="">--Select Unit--</option>
                                            <?php foreach($prod_unit as $row_unit) { ?>
                                            <option value="<?=$row_unit->unit_id;?>"> <?=$row_unit->unit_name;?></option>
                                            <?php } ?>
                                          </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">        
                                                 
                                    <div class="form-group">
                                        <div class="form-group <?=(!empty(form_error('select_category')))?'has-error':'';?>">
                                        <label for="select_category" class="control-label">* Category</label>
                                        <select class="form-control selectpicker" data-live-search="true" tabindex="7" name="select_category" id="select_category" required>
                                            <option value="">--Select Category--</option>
                                            <?php foreach($parent_category as $row_parent) { ?>
                                            <option value="<?=$row_parent->category_id;?>"> <?=$row_parent->category_name;?></option>
                                            <?php } ?>
                                        </select>                                           
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group <?=(!empty(form_error('select_sub')))?'has-error':'';?>">
                                        <label for="select_sub" class="control-label">* Sub Category </label>
                                        <?php echo form_error('select_sub'); ?>
                                        <select class="form-control selectpicker" data-live-search="true" tabindex="8" name="select_sub" id="select_sub" required>
                                        <!-- fungsi untuk menampilkan data pada select list-->
                                            <option value="">---</option>
                                            <?php foreach($sub_category as $row_sub) { ?>
                                            <option value="<?=$row_sub->category_id;?>" class="<?=$row_sub->parent_id; ?>"> <?=$row_sub->category_name;?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>                                       
                                    <div class="form-group <?=(!empty(form_error('product_desc_short')))?'has-error':'';?>">
                                        <label for="product_desc_short" class="control-label">* Short Description </label>
                                        <textarea class="textarea form-control" tabindex="9" rows="8" name="product_desc_short" id="product_desc_short"><?=$product_desc_short;?></textarea>
                                    </div> 
                                    </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- image uploads -->
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">Product Images</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                    <!-- image product --> 
                                        <ul class="add_product_image mt-15 clearfix" id="image-list">
                                            <li id="pickfile-container">
                                                <div class="file photo-container">
                                                    <input type="file" id="img-input" name="prodct_image1"/>
                                                    <div id="img-preview"></div>
                                                    <div class="picture_bar" id="picture_bar" style="display: block;">                                                        
                                                        <!-- picture_bar -->
                                                    </div>
                                                </div>
                                            </li>
                                            
                                            <li id="pickfile-container">
                                                <div class="file photo-container">
                                                    <input type="file" id="img-input2" name="prodct_image2"/>
                                                    <div id="img-preview2"></div>
                                                    <div class="picture_bar" id="picture_bar2" style="display: block;">                                                        
                                                        <!-- picture_bar -->
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="pickfile-container">
                                                <div class="file photo-container">
                                                    <input type="file" id="img-input3" name="prodct_image3"/>
                                                    <div id="img-preview3"></div>
                                                    <div class="picture_bar" id="picture_bar3" style="display: block;">                                                        
                                                        <!-- picture_bar -->
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="pickfile-container">
                                                <div class="file photo-container">
                                                    <input type="file" id="img-input4" name="prodct_image4"/>
                                                    <div id="img-preview4"></div>
                                                    <div class="picture_bar" id="picture_bar4" style="display: block;">                                                        
                                                        <!-- picture_bar -->
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="pickfile-container">
                                                <div class="file photo-container">
                                                    <input type="file" id="img-input5" name="prodct_image5"/>
                                                    <div id="img-preview5"></div>
                                                    <div class="picture_bar" id="picture_bar5" style="display: block;">                                                        
                                                        <!-- picture_bar -->
                                                    </div>
                                                </div>
                                            </li>
                                            <li id="pickfile-container">
                                                <div class="file photo-container">
                                                    <input type="file" id="img-input6" name="prodct_image6"/>
                                                    <div id="img-preview6"></div>
                                                    <div class="picture_bar" id="picture_bar6" style="display: block;">                                                        
                                                        <!-- picture_bar -->
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>                                      
                                        <!-- /.image product -->                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. image uploads -->
                    <div class="panel panel-default">
                        <div class="panel-heading">Product Description</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group <?=(!empty(form_error('product_desc_long')))?'has-error':'';?>">
                                        <textarea class="textarea form-control" tabindex="10" rows="8" name="product_desc_long" id="product_desc_long"><?=$product_desc_long;?></textarea>
                                    </div> 
                                    <button tabindex="11" type="submit" class="btn btn-default">Save</button>
                                    <button tabindex="12" type="reset" class="btn btn-default">Reset</button>                             
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>                   
                </form>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
<!-- #store form setting-->


<!-- jQuery -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/sbadmin/dist/js/sb-admin-2.js"></script>
    <script src="<?php echo base_url(); ?>assets/sbadmin/js/jquery.chained.min.js"></script>
    <script type="text/javascript">
        $("#select_sub").chained("#select_category"); 
        
        $('#select_category').on('change', function () {
            var opValue = $(this).val();
            console.log(opValue);
            $('#select_sub').val(opValue);
            $('#select_sub').selectpicker('refresh');
        });
    </script>
    <!-- validator -->
    <script src="<?php echo base_url();?>assets/dist/validator.min.js"></script>
    <script src="<?php echo base_url();?>assets/dist/validator.js"></script>
    <!-- uploader -->
    <!-- WYSIWYG -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/WYSIWYG/editor.js"></script>
    <!-- bootstrap-select -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <!-- image-preview -->
    <script src="<?php echo base_url();?>assets/sbadmin/dist/js/image-preview.js"></script>

    <script>
        /* WYSIWYG settings*/
        $(document).ready(function() {
                $("#product_desc_long").Editor(
                        {'bold':true,
                         'insert_img':false,
                         'print':false,
                         'rm_format':false});
        });
        
         // validation
        $(document).ready(function () {
          //called when key is pressed in textbox
          $("#ProductWeight").keypress(function (e) {
             //if the letter is not digit then display error and don't type anything
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message       
                       return false;
            }
           });
           
           $("#ProductWeightPackaging").keypress(function (e) {
             //if the letter is not digit then display error and don't type anything
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message       
                       return false;
            }
           });
        });
    </script>

</body>
</html>