
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$this->lang->line("common_title");?></title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sbadmin/bower_components/bootstrap/dist/css/bootstrap.min.css">
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/dist/test.css">

<link rel="stylesheet" href="https://www.tokopedia.com/css/dv3-font-tokopedia-20140421-min.css?v=1446626703" type="text/css"/>

<!--[if lt IE 9]>
<script src="https://ecs1.tokopedia.net/scripts/html5shiv.min.js?v=1399531321"></script>
<![endif]-->
</head>

<body>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
            <div class="row-fluid">
            <div class="span9">
                <ul class="add_product_image mt-15 clearfix" id="image-list">
                    <li id="pic-47724470" class="exist-image">
                        <div class="file photo-container">
                            <img class="hide image-large" src="https://ecs7.tokopedia.net/img/cache/300/product-1/2016/1/3/6458305/6458305_6f135060-2266-4682-b587-40aed6c158bc.jpg">
                            <img class="image-preview" src="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/1/3/6458305/6458305_6f135060-2266-4682-b587-40aed6c158bc.jpg">
                            <div class="picture_bar" style="display: block;">
                                <span data-original-title="Foto Utama" class="primary_picture picture_active" data-toggle="tooltip"></span>
                                <span data-original-title="Deskripsi Gambar" class="edit_picture edit_picture_exist" data-toggle="tooltip"><a href=""></a></span>
                            </div>
                            <input value="" type="hidden">
                        </div>
                    </li>
                    <li id="pic-47724471" class="exist-image">
                        <div class="file photo-container">
                            <img class="hide image-large" src="https://ecs7.tokopedia.net/img/cache/300/product-1/2016/1/3/6458305/6458305_8db9c67c-41d2-4634-b989-a0e6c438fd81.jpg">
                            <img class="image-preview" src="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/1/3/6458305/6458305_8db9c67c-41d2-4634-b989-a0e6c438fd81.jpg">
                            <div class="picture_bar" style="display: block;">
                                <span data-original-title="Foto Utama" class="primary_picture " data-toggle="tooltip"></span>
                                <span data-original-title="Deskripsi Gambar" class="edit_picture edit_picture_exist" data-toggle="tooltip"><a href=""></a></span>
                                <span data-original-title="Hapus Gambar" class="delete_picture delete_picture_exist" data-toggle="tooltip"><a href=""></a></span>
                            </div>
                            <input value="" type="hidden"></div>
                    </li>
                    <li id="pickfile-container" class="">
                        <div style="z-index: 1;" id="pickfiles-nav1" class="file"></div>
                        <div style="position: absolute; top: 0px; left: 0px; width: 80px; height: 80px; overflow: hidden; z-index: 0;" class="moxie-shim moxie-shim-html5" id="html5_1ado9cgookg710gsmf159lnmv3_container">
                            <input id="html5_1ado9cgookg710gsmf159lnmv3" style="font-size: 999px; opacity: 0; position: absolute; top: 0px; left: 0px; width: 100%; height: 100%;" accept="image/jpeg,image/png" type="file">
                        </div>
                    </li>
                </ul>
            </div>
        </div> 
    </div>     
        </div>
    </div>
        <script src="<?php echo base_url();?>assets/dist/test.js"></script>
</body>
</html>
