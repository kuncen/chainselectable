<?php
class product_model extends CI_Model {

    
    private $_table = 'product';
    
    
    // get product list
    public function get_productList(){
        
        $this->db->select("*")
                 ->from($this->_table)
                 ->join('category',$this->_table.'.category_id = category.category_id'); 
        $query = $this->db->get();
        return $query->result_array();
        
    }
    
    public function get_Category($id){
        
        $this->db->select('*');
        if($id==0){
            $this->db->where('parent_id', $id);
        }
        $query=$this->db->get('category');
        Return $query->result();
        
    }    
     
    public function get_prodUnit(){
        
        $this->db->select('*');       
        $query=$this->db->get('unit');
        Return $query->result();
        
    }
      
    public function add_data($data){
          
        return $this->db->insert($this->_table, $data);
        
    }
    
    
    public function update_data($id,$data){
        
        $this->db->trans_begin(); //transaction initialize
        
        $this->db->where('category_id', $id);
        $this->db->update($this->_table, $data);
        
        if($this->db->trans_status()===false){
            $this->db->trans_rollback();
            return false;    
        }
        else{
            $this->db->trans_complete();
            return true;
        }
        
    }
    
    
    public function delete_data($data){
        
        $this->db->trans_begin(); //transaction initialize
        $par_id = $this->get_detailCategory($data['category_id']);
        if($this->get_numProductCategory($par_id->category_id) == 0){
            $this->db->delete($this->_table,$data);      
            if($this->db->trans_status()===false){

                $this->db->trans_rollback();
                return false;    

            }else{

                $this->db->trans_complete();
                return true;
            }
        }
        else{
            $this->db->trans_rollback();
            return false;
        }
        
    }
    
}