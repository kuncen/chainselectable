<?php
class category_model extends CI_Model {

    
    private $_table = 'category';
    
    
    // get category
    public function get_category($id){
        
        $sql = "SELECT * FROM {$this->_table}
                WHERE parent_id = {$id}";
        
        $ret['rows']=$this->db->query($sql)->result();
        $ret['total']=$this->db->query($sql)->num_rows();
        return $ret;
        
    }       
    
        // count product by category
    public function count_productByCategory($id){
        
        $sql = "SELECT * FROM product
                WHERE category_id = {$id}";
        
        return $this->db->query($sql)->num_rows();
                      
    }   
    
    public function get_detailCategory($id){
        
        $sql = "SELECT * FROM {$this->_table}
                WHERE category_id = {$id}";
        
        return $this->db->query($sql)->row();       
        
    }
    
    
    public function save_data($data){
          
        return $this->db->insert($this->_table, $data);
        
    }
    
    
    public function update_data($id,$data){
        
        $this->db->trans_begin();    
        $this->db->where('category_id', $id);
        $this->db->update($this->_table, $data);
        
        if($this->db->trans_status()===false){
            $this->db->trans_rollback();
            return false;    
        }
        else{
            $this->db->trans_complete();
            return true;
        }
        
    }
    
    
    public function delete_data($data){
        
        $this->db->trans_begin(); //transaction initialize
        $par_id = $this->get_detailCategory($data['category_id']);
        // cek dependency to child category
        $child = $this->get_category($par_id->category_id);
        if($child['total'] == 0){
            // cek dependency to product
            if($this->count_productByCategory($par_id->category_id) == 0){
                $this->db->delete($this->_table,$data);      
                if($this->db->trans_status()===true){
                    
                    $this->db->trans_complete();
                    return true;
                    
                }
                else{

                    $this->db->trans_rollback();
                    return false; 
                }
            }
            else{
                $this->db->trans_rollback();
                return false;                
            }
        }
        else{
            $this->db->trans_rollback();
            return false;
        }
        
    }          
}