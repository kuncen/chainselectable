 <!-- store form setting  -->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Store Profile</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <?php if($val == 'FAILED'){?>
                <div class="alert alert-danger min-form">
                    <h6><?=$this->lang->line('err_title');?></h6>
                    <?php echo validation_errors(); ?>
                </div>
                <?php } elseif($this->uri->segment(4,1)=='s'){ ?>
                    <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    Store Profile has been updated successfully !!
                </div>
                <?php } ?>
                
                <form role="form" method="post" action="<?=base_url();?>setting/store-profile/do-update" data-toggle="validator">
                    <input type="hidden" name="update" value="update">
                    <div class="panel panel-default">                   
                        <div class="panel-heading"> Store Information</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group <?=(!empty(form_error('store_name')))?'has-error':'';?>">
                                        <label for="StoreName" class="control-label">* Store Name</label>
                                        <input type="text" class="form-control" tabindex="1" name="store_name" id="StoreName" value="<?=$store_name;?>" pattern="<?=MY_Form_validation::$alpha_extra_pattern;?>" data-error="<?=$this->lang->line('err_store_name');?>" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group <?=(!empty(form_error('store_website')))?'has-error':'';?>">
                                        <label for="StoreWebsite" class="control-label">* Website</label>
                                        <input class="form-control" tabindex="2" name="store_website" id="StoreWebsite" value="<?=$store_website;?>" pattern="<?=MY_Form_validation::$url_pattern;?>" data-error="<?=$this->lang->line('err_store_website');?>" required>
                                        <div class="help-block with-errors"></div>
                                    </div>                                                                                                         
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                    <div class="form-group <?=(!empty(form_error('store_description')))?'has-error':'';?>">
                                        <label for="StoreDescription" class="control-label">Description</label>
                                        <textarea class="form-control" tabindex="3" rows="4" name="store_description" id="StoreDescription" data-error="<?=$this->lang->line('err_store_description');?>"><?=$store_description;?></textarea>
                                    </div>                                
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Store Address</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group <?=(!empty(form_error('store_addr_floor')))?'has-error':'';?>">
                                        <label for="AddrFloor" class="control-label">Door No/ Floor</label>
                                        <input class="form-control" tabindex="4" name="store_addr_floor" id="AddrFloor" value="<?=$store_addr_floor;?>" pattern="<?=MY_Form_validation::$alpha_extra_pattern;?>" data-error="<?=$this->lang->line('err_store_addr_floor');?>">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group <?=(!empty(form_error('store_addr_building')))?'has-error':'';?>">
                                        <label for="AddrBuilding" class="control-label">Landmark / Building</label>
                                        <input class="form-control" tabindex="5" name="store_addr_building" id="AddrBuilding" value="<?=$store_addr_building;?>" pattern="<?=MY_Form_validation::$alpha_extra_pattern;?>" data-error="<?=$this->lang->line('err_store_addr_building');?>">
                                        <div class="help-block with-errors"></div>
                                    </div> 
                                    <div class="form-group <?=(!empty(form_error('store_addr_street')))?'has-error':'';?>">
                                        <label for="AddrStreet" class="control-label">* Street</label>
                                        <input class="form-control" tabindex="6" name="store_addr_street" id="AddrStreet" value="<?=$store_addr_street;?>" pattern="<?=MY_Form_validation::$alpha_extra_pattern;?>" data-error="<?=$this->lang->line('err_store_addr_street');?>" required>
                                        <div class="help-block with-errors"></div>
                                    </div>                                   
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                    <div class="form-group <?=(!empty(form_error('select_state')))?'has-error':'';?>">
                                        <label for="select_state" class="control-label">* State / Province</label>
                                        <select class="form-control selectpicker" data-live-search="true" tabindex="7" name="select_state" id="select_state" required>
                                            <option value="">--Select Provinces--</option>
                                            <!-- fungsi untuk menampilkan data pada select list-->
                                            <?php foreach($states as $row_state) { 
                                            $row_state->state_id == $select_state ? $select = 'selected' : $select='';    
                                            ?>
                                            <option value="<?=$row_state->state_id;?>" <?=$select;?>><?=$row_state->state_name;?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group <?=(!empty(form_error('select_city')))?'has-error':'';?>">
                                        <label for="select_city" class="control-label">* City </label>
                                        <?php echo form_error('select_city'); ?>
                                        <select class="form-control selectpicker" data-live-search="true" tabindex="8" name="select_city" id="select_city" required>
                                        <!-- fungsi untuk menampilkan data pada select list-->
                                        <?php foreach($cities as $row_city) { 
                                            $row_city->city_id == $select_city ? $select = 'selected' : $select='';
                                        ?>
                                        <option value="<?=$row_city->city_id?>" class="<?=$row_city->state_id; ?>" <?=$select;?>> <?=$row_city->city_name?></option>
                                        <?php } ?>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div> 
                                    <div class="form-group <?=(!empty(form_error('store_addr_postal')))?'has-error':'';?>">
                                        <label for="AddrPostal" class="control-label">* Postal Code</label>
                                        <input class="form-control" tabindex="9" name="store_addr_postal" id="AddrPostal" value="<?=$store_addr_postal;?>" pattern="<?=MY_Form_validation::$postal_pattern;?>" data-error="<?=$this->lang->line('err_store_addr_postal');?>" maxlength="5" required>
                                        <div class="help-block with-errors"></div>
                                    </div>                                   
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Contact Information</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group <?=(!empty(form_error('store_ct_email')))?'has-error':'';?>">
                                        <label for="CtEmail" class="control-label">* Email Address</label>
                                        <input type="email" class="form-control" tabindex="10" name="store_ct_email" id="CtEmail" value="<?=$store_ct_email;?>" pattern="<?=MY_Form_validation::$email_pattern;?>" data-error="<?=$this->lang->line('err_store_ct_email');?>" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group <?=(!empty(form_error('store_ct_bbm')))?'has-error':'';?>">
                                        <label for="CtBBM" class="control-label">PIN BBM</label>
                                        <input class="form-control" tabindex="11" name="store_ct_bbm" id="CtBBM" value="<?=$store_ct_bbm;?>" pattern="<?=MY_Form_validation::$alphanum_pattern;?>" data-error="<?=$this->lang->line('err_store_ct_bbm');?>" maxlength="8">
                                        <div class="help-block with-errors"></div>
                                    </div> 
                                    <div class="form-group <?=(!empty(form_error('store_ct_phone')))?'has-error':'';?>">
                                        <label for="CtPhone" class="control-label">* Phone Number</label>
                                        <input class="form-control" tabindex="12" name="store_ct_phone" id="CtPhone" value="<?=$store_ct_phone;?>" pattern="<?=MY_Form_validation::$phone_pattern;?>" data-error="<?=$this->lang->line('err_store_ct_phone');?>" maxlength="12" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <button tabindex="16" type="submit" class="btn btn-default">Save</button>
                                    <button tabindex="17" type="reset" class="btn btn-default">Reset</button>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                    <div class="form-group <?=(!empty(form_error('store_ct_wa')))?'has-error':'';?>">
                                        <label for="CtWA" class="control-label">Whatsapp</label>
                                        <input class="form-control" tabindex="13" name="store_ct_wa" value="<?=$store_ct_wa;?>" id="CtWA" pattern="<?=MY_Form_validation::$phone_pattern;?>" data-error="<?=$this->lang->line('err_store_ct_wa');?>" maxlength="12">
                                        <div class="help-block with-errors"></div>
                                    </div> 
                                    <div class="form-group <?=(!empty(form_error('store_ct_line')))?'has-error':'';?>">
                                        <label for="CtLine" class="control-label">Line ID</label>
                                        <input class="form-control" tabindex="14" name="store_ct_line" value="<?=$store_ct_line;?>" pattern="<?=MY_Form_validation::$alpha_extra_pattern;?>" data-error="<?=$this->lang->line('err_store_ct_line');?>">
                                        <div class="help-block with-errors"></div>
                                    </div> 
                                    <div class="form-group <?=(!empty(form_error('store_ct_sms')))?'has-error':'';?>">
                                        <label for="CtSMS" class="control-label">SMS</label>
                                        <input class="form-control" tabindex="15" name="store_ct_sms" value="<?=$store_ct_sms;?>" id="CtSMS" pattern="<?=MY_Form_validation::$phone_pattern;?>" data-error="<?=$this->lang->line('err_store_ct_sms');?>" maxlength="12">
                                        <div class="help-block with-errors"></div>
                                    </div>                                   
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </form>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
<!-- #store form setting-->


<!-- jQuery -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/sbadmin/dist/js/sb-admin-2.js"></script>
    <script src="<?php echo base_url(); ?>assets/sbadmin/js/jquery.chained.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $("#select_city").chained("#select_state"); 
        $('#select_state').on('change', function () {
            var opValue = $(this).val();
            console.log(opValue);
            $('#select_city').val(opValue);
            $('#select_city').selectpicker('refresh');
        });
    </script>
    <script src="<?php echo base_url();?>assets/dist/validator.min.js"></script>
    <script src="<?php echo base_url();?>assets/dist/validator.js"></script>
     <!-- bootstrap-select -->
    <script src="<?php echo base_url();?>assets/sbadmin/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
          //called when key is pressed in textbox
          $("#AddrPostal").keypress(function (e) {
             //if the letter is not digit then display error and don't type anything
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message       
                       return false;
            }
           });
        });
        
        $(document).ready(function () {
          //called when key is pressed in textbox
          $("#CtPhone").keypress(function (e) {
             //if the letter is not digit then display error and don't type anything
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message       
                       return false;
            }
           });
        });
        $(document).ready(function () {
          //called when key is pressed in textbox
          $("#CtWA").keypress(function (e) {
             //if the letter is not digit then display error and don't type anything
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message       
                       return false;
            }
           });
        });
        $(document).ready(function () {
          //called when key is pressed in textbox
          $("#CtSMS").keypress(function (e) {
             //if the letter is not digit then display error and don't type anything
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message       
                       return false;
            }
           });
        });
    </script>

</body>
</html>