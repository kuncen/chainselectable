<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_profile extends MX_Controller{

    private $d;
        
    public function __construct(){

        parent::__construct();
        //if(!$this->session->userdata('is_login'))redirect('login');
        $this->session->set_userdata('menu','');
        $this->load->model('store_model');
        $this->load->model('common_model');
        $this->load->library('MY_Form_validation');
        
        $this->_mod_url = base_url().'setting/store-profile';
        
       
    }
    
    private function _render($view,$data){
        
	$data['store_profile']= $this->store_model->get_profile();
        $data['states']= $this->common_model->get_states();
        $data['cities']= $this->common_model->get_cities();   
        

        $this->load->view('header');
        $this->load->view('sidemenu');
        $this->load->view($view,$data);
        $this->load->view('footer');
        
    }
    
    public function index(){
        $this->_set_fields();
        $this->d['val'] = '';
        $this->_render('store_profile',$this->d);
    }
    
    public function do_update() {
        $this->form_validation();

        if ($this->form_validation->run() == FALSE){
            $this->d['val'] = 'FAILED';
            $this->_render('store_profile',$this->d);
        }
        else{
            $data = array (
                'store_name'            => $this->input->post('store_name'),
                'store_description'     => $this->input->post('store_description'),
                'store_website'         => $this->input->post('store_website'),
                'store_addr_floor'      => $this->input->post('store_addr_floor'),
                'store_addr_building'   => $this->input->post('store_addr_building'),
                'store_addr_street'     => $this->input->post('store_addr_street'),
                'store_addr_state_id'   => $this->input->post('select_state'),
                'store_addr_city_id'    => $this->input->post('select_city'),
                'store_addr_postal'     => $this->input->post('store_addr_postal'),
                'store_ct_email'        => $this->input->post('store_ct_email'),
                'store_ct_bbm'          => $this->input->post('store_ct_bbm'),
                'store_ct_wa'           => $this->input->post('store_ct_wa'),
                'store_ct_line'         => $this->input->post('store_ct_line'),
                'store_ct_sms'          => $this->input->post('store_ct_sms'),
                'store_ct_phone'        => $this->input->post('store_ct_phone')
                //'store_id'              => $this->input->post('store_id')
                );     
            $result = $this->store_model->update_data($data);
            if($result){
                redirect('setting/store-profile/index/s');
            }
        }      
    }
    
    private function form_validation(){
        
        $this->form_validation->set_error_delimiters('<div><small>', '</small></div>');
        $this->form_validation->set_rules('store_name','Store Name','required|alpha_extra');     
        $this->form_validation->set_rules('store_website','Store Website','required|valid_url');
        $this->form_validation->set_rules('store_description','Store Description','alpha_extra');
        $this->form_validation->set_rules('store_addr_floor','Door No/ Floor level','alpha_extra');
        $this->form_validation->set_rules('store_addr_building','Landmark / Building Name','alpha_extra');
        $this->form_validation->set_rules('store_addr_street','Street name','required|alpha_extra');
        $this->form_validation->set_rules('select_state','State / Provinces','required');
        $this->form_validation->set_rules('select_city','City','required');
        $this->form_validation->set_rules('store_addr_postal','Postal Code','required|postal');
        $this->form_validation->set_rules('store_ct_email','Email Address','required|valid_email');
        $this->form_validation->set_rules('store_ct_bbm','PIN BBM','alpha_numeric|exact_length[8]');
        $this->form_validation->set_rules('store_ct_phone','Phone Number','required|numeric|min_length[10]|max_length[12]');
        $this->form_validation->set_rules('store_ct_wa','Whatsapp Number','numeric|min_length[10]|max_length[12]');
        $this->form_validation->set_rules('store_ct_line','Line ID','alpha_extra');
        $this->form_validation->set_rules('store_ct_sms','SMS Number','numeric|min_length[10]|max_length[12]');
        
        // Return the result validation to form
        $store_profile = $this->store_model->get_profile();    
        $this->d['store_name']             = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_name'):$store_profile->store_name;
        $this->d['store_website']          = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_website'):$store_profile->store_website;
        $this->d['store_description']      = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_description'):$store_profile->store_description;
        $this->d['store_addr_floor']       = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_addr_floor'):$store_profile->store_addr_floor;
        $this->d['store_addr_building']    = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_addr_building'):$store_profile->store_addr_building;
        $this->d['store_addr_street']      = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_addr_street'):$store_profile->store_addr_street;       
        $this->d['select_state']           = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('select_state'):$store_profile->store_addr_state_id;
        $this->d['select_city']            = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('select_city'):$store_profile->store_addr_city_id;
        $this->d['store_addr_postal']      = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_addr_postal'):$store_profile->store_addr_postal;
        $this->d['store_ct_email']         = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_ct_email'):$store_profile->store_ct_email;
        $this->d['store_ct_bbm']           = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_ct_bbm'):$store_profile->store_ct_bbm;
        $this->d['store_ct_phone']         = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_ct_phone'):$store_profile->store_ct_phone;
        $this->d['store_ct_line']          = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_ct_line'):$store_profile->store_ct_line;
        $this->d['store_ct_sms']           = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_ct_sms'):$store_profile->store_ct_sms;      
        $this->d['store_ct_wa']            = ($this->form_validation->run() == FALSE)?$this->form_validation->set_value('store_ct_wa'):$store_profile->store_ct_wa;
    }
    
    private function _set_fields(){
        
        $store_profile = $this->store_model->get_profile(); 
        $this->d['store_name']             = $store_profile->store_name;
        $this->d['store_website']          = $store_profile->store_website;
        $this->d['store_description']      = $store_profile->store_description;
        $this->d['store_addr_floor']       = $store_profile->store_addr_floor;
        $this->d['store_addr_building']    = $store_profile->store_addr_building;
        $this->d['store_addr_street']      = $store_profile->store_addr_street;       
        $this->d['select_state']           = $store_profile->store_addr_state_id;
        $this->d['select_city']            = $store_profile->store_addr_city_id;
        $this->d['store_addr_postal']      = $store_profile->store_addr_postal;
        $this->d['store_ct_email']         = $store_profile->store_ct_email;
        $this->d['store_ct_bbm']           = $store_profile->store_ct_bbm;
        $this->d['store_ct_phone']         = $store_profile->store_ct_phone;
        $this->d['store_ct_line']          = $store_profile->store_ct_line;
        $this->d['store_ct_sms']           = $store_profile->store_ct_sms;      
        $this->d['store_ct_wa']            = $store_profile->store_ct_wa;
    }

}
