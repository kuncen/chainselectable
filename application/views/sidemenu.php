            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="<?=base_url();?>dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>       
                        
                        <li>
                            <a href="#"><i class="fa fa-gears fa-fw"></i> Setting<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=base_url();?>setting/store-profile">Store Profile</a>
                                </li>
                                <li>
                                    <a href="<?=base_url();?>setting/system">System</a>
                                </li>
                                <li>
                                    <a href="<?=base_url();?>setting/user">User Account</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-shopping-cart fa-fw"></i> Purchasing<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="supplier">Supplier</a>
                                </li>
                                <li>
                                    <a href="purchaseorder">Purchase Order</a>
                                </li>                                              
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dollar fa-fw"></i> Selling<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="merchart">Merchart</a>
                                </li>
                                <li>
                                    <a href="customers">Customers</a>
                                </li>
                                <li>
                                    <a href="sales">Sales</a>
                                </li>                                             
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cubes fa-fw"></i> Product<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=base_url();?>product/category">Category</a>
                                </li>
                                <li>
                                    <a href="<?=base_url();?>product/product">Product</a>
                                </li>
                                <li>
                                    <a href="<?=base_url();?>product/stock">Stock</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Report<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="blank.html">Blank Page</a>
                                </li>
                                <li>
                                    <a href="login.html">Login Page</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
        </nav>