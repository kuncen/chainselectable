<?php

$lang["err_title"] = "Check the following errors before submitting :";
$lang["err_store_name"] = "The store name field may only contain alpha-numeric, dashes, dot, comma and spaces";
$lang["err_store_website"] = "The url website doesn't a valid and not in the proper format";
$lang["err_store_description"] = "The store description field may only contain alpha-numeric, dashes, dot, comma and spaces";
$lang["err_store_addr_floor"] = "The floor level or door number field may only contain alpha-numeric, dashes, dot, comma and spaces";
$lang["err_store_addr_building"] = "The building name field may only contain alpha-numeric, dashes, dot, comma and spaces";
$lang["err_store_addr_street"] = "The street name field may only contain alpha-numeric, dashes, dot, comma and spaces";
$lang["err_store_addr_postal"] = "The postal code field must contain a valid indonesian format.";
$lang["err_store_ct_email"] = "The email address doesn't a valid, email address must be in the following format test@example.com";
$lang["err_store_ct_bbm"] = "The PIN BBM doesn't a valid, only allow alpha-numeric character";
$lang["err_store_ct_phone"] = "The Phone number doesn't a valid, only allow numeric character";
$lang["err_store_ct_wa"] = "The Whatsapp number doesn't a valid, only allow numeric character";
$lang["err_store_ct_sms"] = "The Text / SMS number doesn't a valid, only allow numeric character";
$lang["err_store_ct_line"] = "The Line ID field may only contain alpha-numeric, dashes, dot, comma and spaces";
$lang["err_category_name"] = "The Category Name field may only contain alpha-numeric, dashes, dot, comma and spaces";

$lang["err_product_name"] = "The Product Name field may only contain alpha-numeric, dashes, dot, comma and spaces";
$lang["err_product_weight"] = "The Product weight doesn't a valid, only allow numeric character";
$lang["err_product_weight_packaging"] = "The Product weight + packaging doesn't a valid, only allow numeric character";