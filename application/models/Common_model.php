<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model {

    
    function get_states(){
        $this->db->select('*');
        //$this->db->where('store_id', $id);
        $this->db->order_by("UPPER(state_name)","ASC");
        $query=$this->db->get('states');
        Return $query->result();
    }
    function get_cities(){
        $this->db->select('*');
       // $this->db->where('city_id', $id);
        $this->db->order_by("UPPER(city_name)","ASC");
        $query=$this->db->get('cities');
        Return $query->result();
    }
} 


