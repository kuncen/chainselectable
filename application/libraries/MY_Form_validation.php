<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// application/libraries/MY_Form_validation.php

class MY_Form_validation extends CI_Form_validation{
    
    public static $alpha_extra_pattern = '^[a-zA-Z0-9\s.\-,]+$';
    public static $postal_pattern = '^[0-9]{5}?$';
    public static $phone_pattern = '^[0-9]{7,12}?$';
    public static $url_pattern = '^((http)(s?))\://([0-9a-zA-Z\-]+\.)+[a-zA-Z]{2,6}?$';
    public static $email_pattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
    public static $alphanum_pattern ='[a-zA-Z0-9]+';
    public function __construct(){
        parent::__construct();
    }
        
    function alpha_extra($str){
          
        if(preg_match('/'.$this::$alpha_extra_pattern.'/', $str)){          
            return TRUE;
        }
        else{
            $this->CI->form_validation->set_message('alpha_extra', 'The %s field may only contain alpha-numeric, dashes, dot, comma and spaces');
            return FALSE;
        }
    }
    function postal($str){
          
        if(preg_match('/'.$this::$postal_pattern.'/', $str)){          
            return TRUE;
        }
        else{
            $this->CI->form_validation->set_message('postal', 'The %s field must contain a valid indonesia format.');
            return FALSE;
        }
    }
}
